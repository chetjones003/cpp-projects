//
// Created by chetjones on 2/12/21.
//

#include "TicTacToeGame.h"
#include <iostream>

using namespace std;

void TicTacToeGame::playGame() {

    clearBoard();

    // Player symbols
    char player1 = 'X';
    char player2 = 'O';

    // Keep track of current player
    char currentPlayer = 'X';

    // Keep track of game status
    bool isDone = false;

    // X Y int to store
    int x, y;

    // keep track of turns
    int turn = 0;

    // Game Loop
    while (!isDone) {
        // Draw the board
        printBoard();

        // Ask User for X Y coord
        x = getXCoord();
        y = getYCoord();
        if (!placeMarker(x, y, currentPlayer)) {
            cout << "That spot is occupied.\n";
        } else {
            // increment turns
            turn++;
            // Checking for victory
            if (checkForVictory(currentPlayer)) {
                cout << "Game is Over!! Player " << currentPlayer << " has won!!\n";
                isDone = true;
            } else if (turn == 9) {
                cout << "It's a tie game!!\n";
                isDone = true;
            }

            // Switch Players
            if (currentPlayer == player1) {
                currentPlayer = player2;
            } else {
                currentPlayer = player1;
            }
        }
    }
}

int TicTacToeGame::getXCoord() {
    // used to check bad input
    bool isInputBad = true;
    // x store value
    int x;

    // Get x from user, check if in range, store value
    while (isInputBad) {
        cout << "Enter the X Coordinate: ";
        cin >> x;

        if (x < 1 || x > 3) {
            cout << "Invalid Coordinate!\n";
        } else {
            isInputBad = false;
        }
    }
    return x - 1;
}

int TicTacToeGame::getYCoord() {
    // used to check bad input
    bool isInputBad = true;
    // y store value
    int y;

    // Get y from user, check if in range, store value
    while (isInputBad) {
        cout << "Enter the Y Coordinate: ";
        cin >> y;

        if (y < 1 || y > 3) {
            cout << "Invalid Coordinate!\n";
        } else {
            isInputBad = false;
        }
    }
    return y - 1;
}

bool TicTacToeGame::placeMarker(int x, int y, char currentPlayer) {
    // If no white space don't place marker
    if (board[y][x] != ' ') {
        return false;
    }

    // Place marker at coord with players symbol
    board[y][x] = currentPlayer;
    return true;
}

bool TicTacToeGame::checkForVictory(char currentPlayer) {
    // Check the rows
    for (auto & i : board) {
        if ((i[0] == currentPlayer) && (i[0] == i[1]) && (i[1] == i[2])) {
            return true;
        }
    }

    // Check the columns
    for (int i = 0; i < 3; i++) {
        if ((board[0][i] == currentPlayer) && (board[0][i] == board[1][i]) && (board[1][i] == board[2][i])) {
            return true;
        }
    }

    // Check the diagonals
    // top left
    if ((board[0][0] == currentPlayer) && (board[0][0] == board[1][1]) && (board[1][1] == board[2][2])) {
        return true;
    }
    // top right
    if ((board[2][0] == currentPlayer) && (board[2][0] == board[1][1]) && (board[1][1] == board[0][2])) {
        return true;
    }

    // No victory yet
    return false;
}

TicTacToeGame::TicTacToeGame() = default;

void TicTacToeGame::clearBoard() {
    // Use whitespace at all board indexes
    for (auto & i : board) {
        for (char & j : i) {
            j = ' ';
        }
    }
}

void TicTacToeGame::printBoard() {
    // Drawing the board
    cout << endl;
    cout << " |1 2 3| \n";
    for (int i = 0; i < 3; i++) {
        cout << " -------\n";
        cout << i + 1 << "|" << board[i][0] << "|" << board[i][1] << "|" << board[i][2] << "|\n";
    }
    cout << " -------\n";
}