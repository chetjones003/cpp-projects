//
// Created by chetjones on 2/12/21.
//

#include <iostream>
#include "TicTacToeGame.h"

using namespace std;

int main() {
    bool isDone = false;
    char input;
    TicTacToeGame game;

    while (!isDone) {
        game.playGame();

        bool inputCheck = false;
        while (!inputCheck) {
            cout << "Would you like to play again? Y/N: ";
            cin >> input;
            if (input == 'y' || input == 'Y') {
                game.playGame();
            } else if (input == 'n' || input == 'N') {
                inputCheck = true;
            } else {
                cout << "Pleas enter Y or N\n";
                inputCheck = false;
            }
        }
        isDone = true;
    }

    return 0;
}
