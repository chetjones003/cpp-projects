//
// Created by chetjones on 2/16/21.
//

#include "../HeaderFiles/Player.h"

Player::Player() {

}

void Player::init(std::string name, int money) {
    _name = name;
    _money = money;
}

void Player::printInventory() {
    std::cout << "\n*** " << _name << "'s Inventory ***" << std::endl;
    std::cout << "Money: " << _money << " gold." << std::endl;

    // Store value for an ordered list
    int i = 0;

    // Creating list iterator
    std::list<Item>::iterator lit;

    // Loop through list items and print
    for (lit = _items.begin(); lit != _items.end(); lit++) {
        std::cout << i << ". " << (*lit).getName() << " x " << (*lit).getCount() << std::endl;
        i++;
    }
}

bool Player::canAffordItem(std::string name, int money) {
    // Creating list iterator
    std::list<Item>::iterator lit;

    // Loop through list items
    for (lit = _items.begin(); lit != _items.end(); lit++) {
        if ((*lit).getName() == name) {
            if ((*lit).getValue() <= money) {
                return true;
            } else {
                return false;
            }
        }
    }
    return false;
}

bool Player::removeItem(std::string name, Item &newItem) {
    // Creating list iterator
    std::list<Item>::iterator lit;

    // Loop through list items
    for (lit = _items.begin(); lit != _items.end(); lit++) {
        // if add item name and list name match add item
        if ((*lit).getName() == name) {
            newItem = (*lit);
            (*lit).removeOne();
            if ((*lit).getCount() == 0) {
                _items.erase(lit);
            }
            return true;
        }
    }
    return false;
}

void Player::addItem(Item newItem) {
    // Creating list iterator
    std::list<Item>::iterator lit;

    // Loop through list items
    for (lit = _items.begin(); lit != _items.end(); lit++) {
        // if add item name and list name match add item
        if ((*lit).getName() == newItem.getName()) {
            (*lit).addOne();
            return;
        }
    }

    // Put new item in list
    _items.push_back(newItem);
}
