//
// Created by chetjones on 2/16/21.
//

#include <iostream>
#include "../HeaderFiles/Shop.h"

Shop::Shop(std::string name, int money) {
    _name = name;
    _money = money;
}

void Shop::printShop() {
    // Welcome message
    std::cout << "\n*** Welcome to " << _name << " ***" << std::endl;
    std::cout << "Money: " << _money << " gold.\n" << std::endl;

    // Store value for an ordered list
    int i = 0;

    // Creating list iterator
    std::list<Item>::iterator lit;

    // Loop through list items and print
    for (lit = _items.begin(); lit != _items.end(); lit++) {
        std::cout << i + 1 << ". " << (*lit).getName() << " x " << (*lit).getCount() << " Price: " << (*lit).getValue() << " gold." << std::endl;
        i++;
    }
}

bool Shop::canAffordItem(std::string name, int money) {
    // Creating list iterator
    std::list<Item>::iterator lit;

    // Loop through list items
    for (lit = _items.begin(); lit != _items.end(); lit++) {
        if ((*lit).getName() == name) {
            if ((*lit).getValue() <= money) {
                return true;
            } else {
                return false;
            }
        }
    }
    return false;
}

bool Shop::purchaseItem(std::string name, Item &newItem) {
    // Creating list iterator
    std::list<Item>::iterator lit;

    // Loop through list items
    for (lit = _items.begin(); lit != _items.end(); lit++) {
        if ((*lit).getName() == name) {
            // Make a copy of item and return it with a count of 1
            newItem = (*lit);

            newItem.setCount(1);
            (*lit).removeOne();

            if ((*lit).getCount() == 0) {
                _items.erase(lit);
            }
            return true;
        }
    }
    return false;
}
void Shop::addItem(Item newItem) {
    // Creating list iterator
    std::list<Item>::iterator lit;

    // Loop through list items
    for (lit = _items.begin(); lit != _items.end(); lit++) {
        // if add item name and list name match add item
        if ((*lit).getName() == newItem.getName()) {
            (*lit).addOne();
            return;
        }
    }

    // Put new item in list
    _items.push_back(newItem);
}
