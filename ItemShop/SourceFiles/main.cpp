#include <iostream>
#include <string>
#include "../HeaderFiles/Shop.h"
#include "../HeaderFiles/Player.h"

// Prototypes
void initShops(std::list<Shop> &shops);
void initPlayer(Player &player);
void enterShop(Player &player, Shop &shop);

int main() {

    std::list<Shop> shops;
    std::list<Shop>::iterator lit;
    Player player;
    std::string shopInput;

    // Initialize
    initPlayer(player);
    initShops(shops);

    // Game loop bool
    bool isDone = false;

    while (!isDone) {
        std::cout << "Shops: \n";

        // store value for ordered list
        int i = 1;

        // Loop over shops and print them
        for (lit = shops.begin(); lit != shops.end(); lit++) {
            std::cout << i << ". " << (*lit).getName() << std::endl;
            i++;
        }

        std::cout << "\nWhat shop would you like to enter?";
        std::cout << "\nType the shop name or q to quit: ";
        getline(std::cin, shopInput);

        bool validShop = false;

        // Loop to check if shop name was entered
        for (lit = shops.begin(); lit != shops.end(); lit++) {
            if ((*lit).getName() == shopInput) {
                enterShop(player, (*lit));
                validShop = true;
            } else if (shopInput == "q" || shopInput == "Q") {
                validShop = true;
                isDone = true;
            }
        }

        if (!validShop) {
            std::cout << "Invalid shop name" << std::endl;
            std::cout << "Press any key to continue...";
            std::cin.get();
        }
    }

    return 0;
}

void initShops(std::list<Shop> &shops) {
    // Bills Leather Shop
    shops.push_back(Shop("Bill's Leather Shop", 500));
    shops.back().addItem(Item("Leather Boots", 40));
    shops.back().addItem(Item("Leather Hat", 10));
    shops.back().addItem(Item("Leather Gloves", 5));
    shops.back().addItem(Item("Leather Leggings", 20));
    shops.back().addItem(Item("Leather Jerkin", 35));



    // The Blacksmith
    shops.push_back(Shop("The Blacksmith", 1500));
    shops.back().addItem(Item("Iron Boots", 40));
    shops.back().addItem(Item("Iron Hat", 10));
    shops.back().addItem(Item("Iron Gloves", 5));
    shops.back().addItem(Item("Iron Leggings", 20));
    shops.back().addItem(Item("Iron Jerkin", 35));



    // Traci's Inn
    shops.push_back(Shop("Traci's Inn", 300));
    shops.back().addItem(Item("Cooked Chicken", 10));
    shops.back().addItem(Item("Roasted Boar", 25));
    shops.back().addItem(Item("Dwarven Ale", 20));
    shops.back().addItem(Item("Candy", 5));
    shops.back().addItem(Item("Old Wine", 5));
    shops.back().addItem(Item("Invisibility Potion", 50));

}

void initPlayer(Player &player) {
    std::cout << "What is your name: ";
    std::string name;
    getline(std::cin, name);
    player.init(name, 100);
    player.addItem(Item("Bronze Sword", 5));
}

void enterShop(Player &player, Shop &shop) {
    bool isDone = false;
    char input;
    std::string sellName;
    std::string buyName;
    Item newItem("NOITEM", 0);

    while (!isDone) {
        shop.printShop();

        player.printInventory();

        std::cout << "\nWould you like to buy, sell, or quit? (B/S/Q): ";
        std::cin >> input;
        std::cin.ignore(64, '\n');
        std::cin.clear();

        if (input == 'b' || input == 'B') { // buy
            std::cout << "Enter the item you wish to buy: ";
            getline(std::cin, buyName);

            if (shop.canAffordItem(buyName, player.getMoney())) {
                if (shop.purchaseItem(buyName, newItem)) {
                    player.addMoney(-newItem.getValue());
                    player.addItem(newItem);
                    shop.addMoney(newItem.getValue());
                } else {
                    std::cout << "That is an invalid item name!" << std::endl;
                    std::cout << "Press any key to continue...";
                    std::cin.get();
                }
            } else {
                std::cout << "You don't have enough money!" << std::endl;
                std::cout << "Press any key to continue...";
                std::cin.get();
            }

        } else if (input == 's' || input == 'S') { // sell
            std::cout << "Enter the item you wish to sell: ";
            getline(std::cin, sellName);

            if (player.canAffordItem(sellName, shop.getMoney())) {
                if (player.removeItem(sellName, newItem)) {
                    shop.addMoney(-newItem.getValue());
                    shop.addItem(newItem);
                    player.addMoney(newItem.getValue());
                } else {
                    std::cout << "That is an invalid item name!" << std::endl;
                    std::cout << "Press any key to continue...";
                    std::cin.get();
                }
            } else {
                std::cout << "The shop doesn't have enough money!!" << std::endl;
                std::cout << "Press any key to continue...";
                std::cin.get();
            }
        } else if (input == 'q' || input == 'Q') { // quit
            return;

        } else { // bad input
            std::cout << "Input was invalid!";
            std::cout << "Press any key to continue...";
            std::cin.get();
            return;
        }
    }
}