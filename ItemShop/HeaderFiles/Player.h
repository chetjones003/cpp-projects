//
// Created by chetjones on 2/16/21.
//

#pragma once

#include <iostream>
#include <string>
#include <list>
#include "Item.h"

class Player {
public:
    Player();

    void init(std::string name, int money);

    void printInventory();
    bool canAffordItem(std::string name, int money);
    bool removeItem(std::string name, Item &newItem);
    void addItem(Item newItem);
    void addMoney(int money) { _money += money; }

    // Getters
    int getMoney() { return _money; }
    void subtractMoney(int amount) { _money -= amount; }

private:
    std::string _name;
    std::list<Item> _items;
    int _money;

};


