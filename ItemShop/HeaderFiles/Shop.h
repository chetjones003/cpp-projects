//
// Created by chetjones on 2/16/21.
//

#include <string>
#include <list>
#include "Item.h"

#pragma once

class Shop {
public:
    Shop(std::string name, int money);

    void printShop();
    bool canAffordItem(std::string name, int money);
    bool purchaseItem(std::string name, Item &newItem);
    void addItem(Item newItem);

    void addMoney(int money) { _money += money; }

    // Getters
    int getMoney() { return _money; }
    std::string getName() { return  _name; }

private:
    std::string _name;
    std::list<Item> _items;
    int _money;

};


