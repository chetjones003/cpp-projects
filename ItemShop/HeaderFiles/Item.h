//
// Created by chetjones on 2/14/21.
//

#pragma once

#include <iostream>
#include <string>



class Item {
public:
    Item(std::string name, int value);

    void addOne();
    void removeOne();

    // Getters
    std::string getName() { return _name; }
    int getValue() { return _value; }
    int getCount() { return _count; }

    // Setters
    void setCount(int count) { _count = count; }

private:
    std::string _name;
    int _value;
    int _count;
};


