#include <iostream>
#include <random>
#include <ctime>

using namespace std;

int getNumHumans();
int getNumSkeletons();
void simulateBattle(int &numHumans, int &numSkeletons);
void printResults(int numHumans, int numSkeletons, int startHumans, int startSkeletons);

int main()
{
    // Human Stats
    int numHumans;
    int startHumans;

    // Skeleton Stats
    int numSkeletons;
    int startSkeletons;

    cout << "*** Humans vs. Skeletons! ***\n";

    // Get number of humans and initialize human start value
    numHumans = getNumHumans();
    startHumans = numHumans;

    // Get number of skeletons and initialize skeleton start value
    numSkeletons = getNumSkeletons();
    startSkeletons = numSkeletons;

    // Simulate Battle
    simulateBattle(numHumans, numSkeletons);

    // Battle Results
    printResults(numHumans, numSkeletons, startHumans, startSkeletons);

    cin.get();
    cout << "Press any key to exit...";
    cin.get();
    return 0;
}

int getNumHumans() {
    // User Input to determine number of humans
    int numHumans;
    cout << "Enter Number of Humans: ";
    cin >> numHumans;
    return numHumans;
}

int getNumSkeletons() {
    // User Input to determine number of skeletons
    int numSkeletons;
    cout << "Enter Number of Skeletons: ";
    cin >> numSkeletons;
    return numSkeletons;
}

void simulateBattle(int &numHumans, int &numSkeletons) {

    // random number set up
    static default_random_engine randomEngine(time(nullptr));
    uniform_real_distribution<float> attack(0.0f, 1.0f);

    // initializing whose turn it is
    char turn = 'H';

    // Human stats
    float humanAttack = 0.6f;
    float humanDamage = 200.0f;
    float humanHealth = 250.0f;
    float currentHumanHealth = humanHealth;

    // Skeleton stats
    float skeletonAttack = 0.2f;
    float skeletonHealth = 50.0f;
    float skeletonDamage = 40.0f;
    float currentSkeletonHealth = skeletonHealth;

    float attackResult;

    cout << "\nBeginning combat\n\n";

    // combat while loop
    while ((numHumans > 0) && (numSkeletons > 0)) {
        // humans turn
        if (turn == 'H') {
            // get attack result
            attackResult = attack(randomEngine);

            // check if attack was successful
            if (attackResult <= humanAttack) {
                currentSkeletonHealth -= humanDamage;

                // check if skeleton died
                if (currentSkeletonHealth <= 0) {
                    numSkeletons --;
                    currentSkeletonHealth = skeletonHealth;
                }
            }

            // switch turn to skeleton
            turn = 'S';

        } else {
            // get attack result
            attackResult = attack(randomEngine);// check if attack was successful
            if (attackResult <= skeletonAttack) {
                currentHumanHealth -= skeletonDamage;

                // check if human died
                if (currentHumanHealth <= 0) {
                    numHumans--;
                    currentHumanHealth = humanHealth;
                }
            }// switch turn to humans
            turn = 'H';
        }
    }

}

void printResults(int numHumans, int numSkeletons, int startHumans, int startSkeletons) {
    // Results
    cout << "\nBattle is over!\n";

    if (numHumans > 0) {
        cout << "Humans Won!\n";
        cout << "There are " << numHumans << " humans left.\n";
    } else {
        cout << "Skeletons Won!\n";
        cout << "There are " << numSkeletons << " skeletons left.\n";
    }

    // Kill count
    cout << startHumans - numHumans << " humans died\n";
    cout << startSkeletons - numSkeletons << " skeletons died\n\n\n";
}
